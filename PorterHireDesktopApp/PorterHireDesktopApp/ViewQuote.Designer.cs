﻿namespace PorterHireDesktopApp
{
    partial class ViewQuote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblViewQuote = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.VQFilter = new System.Windows.Forms.ComboBox();
            this.VQClear = new System.Windows.Forms.Button();
            this.VQParam = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.VQSortBy = new System.Windows.Forms.ComboBox();
            this.VQGo = new System.Windows.Forms.Button();
            this.VQlistQuote = new System.Windows.Forms.ListBox();
            this.VQGenReport = new System.Windows.Forms.Button();
            this.VQDisplayAll = new System.Windows.Forms.Button();
            this.VQBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblViewQuote
            // 
            this.lblViewQuote.AutoSize = true;
            this.lblViewQuote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblViewQuote.Location = new System.Drawing.Point(12, 9);
            this.lblViewQuote.Name = "lblViewQuote";
            this.lblViewQuote.Size = new System.Drawing.Size(91, 20);
            this.lblViewQuote.TabIndex = 17;
            this.lblViewQuote.Text = "View Quote";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(54, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Search Filter";
            // 
            // VQFilter
            // 
            this.VQFilter.FormattingEnabled = true;
            this.VQFilter.Location = new System.Drawing.Point(58, 119);
            this.VQFilter.Name = "VQFilter";
            this.VQFilter.Size = new System.Drawing.Size(121, 21);
            this.VQFilter.TabIndex = 19;
            // 
            // VQClear
            // 
            this.VQClear.Location = new System.Drawing.Point(394, 117);
            this.VQClear.Name = "VQClear";
            this.VQClear.Size = new System.Drawing.Size(116, 23);
            this.VQClear.TabIndex = 20;
            this.VQClear.Text = "Clear";
            this.VQClear.UseVisualStyleBackColor = true;
            this.VQClear.Click += new System.EventHandler(this.VQClear_Click);
            // 
            // VQParam
            // 
            this.VQParam.Location = new System.Drawing.Point(226, 119);
            this.VQParam.Name = "VQParam";
            this.VQParam.Size = new System.Drawing.Size(134, 20);
            this.VQParam.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(222, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "Search Params";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(54, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 20);
            this.label3.TabIndex = 23;
            this.label3.Text = "Sort By";
            // 
            // VQSortBy
            // 
            this.VQSortBy.FormattingEnabled = true;
            this.VQSortBy.Location = new System.Drawing.Point(58, 231);
            this.VQSortBy.Name = "VQSortBy";
            this.VQSortBy.Size = new System.Drawing.Size(121, 21);
            this.VQSortBy.TabIndex = 24;
            // 
            // VQGo
            // 
            this.VQGo.Location = new System.Drawing.Point(226, 229);
            this.VQGo.Name = "VQGo";
            this.VQGo.Size = new System.Drawing.Size(116, 23);
            this.VQGo.TabIndex = 25;
            this.VQGo.Text = "Go";
            this.VQGo.UseVisualStyleBackColor = true;
            this.VQGo.Click += new System.EventHandler(this.VQGo_Click);
            // 
            // VQlistQuote
            // 
            this.VQlistQuote.FormattingEnabled = true;
            this.VQlistQuote.Location = new System.Drawing.Point(58, 284);
            this.VQlistQuote.Name = "VQlistQuote";
            this.VQlistQuote.Size = new System.Drawing.Size(702, 420);
            this.VQlistQuote.TabIndex = 26;
            // 
            // VQGenReport
            // 
            this.VQGenReport.Location = new System.Drawing.Point(644, 734);
            this.VQGenReport.Name = "VQGenReport";
            this.VQGenReport.Size = new System.Drawing.Size(116, 23);
            this.VQGenReport.TabIndex = 27;
            this.VQGenReport.Text = "Generate Report";
            this.VQGenReport.UseVisualStyleBackColor = true;
            // 
            // VQDisplayAll
            // 
            this.VQDisplayAll.Location = new System.Drawing.Point(58, 734);
            this.VQDisplayAll.Name = "VQDisplayAll";
            this.VQDisplayAll.Size = new System.Drawing.Size(116, 23);
            this.VQDisplayAll.TabIndex = 28;
            this.VQDisplayAll.Text = "Display All";
            this.VQDisplayAll.UseVisualStyleBackColor = true;
            // 
            // VQBack
            // 
            this.VQBack.Location = new System.Drawing.Point(856, 734);
            this.VQBack.Name = "VQBack";
            this.VQBack.Size = new System.Drawing.Size(116, 23);
            this.VQBack.TabIndex = 29;
            this.VQBack.Text = "Back";
            this.VQBack.UseVisualStyleBackColor = true;
            this.VQBack.Click += new System.EventHandler(this.VQBack_Click);
            // 
            // ViewQuote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.VQBack);
            this.Controls.Add(this.VQDisplayAll);
            this.Controls.Add(this.VQGenReport);
            this.Controls.Add(this.VQlistQuote);
            this.Controls.Add(this.VQGo);
            this.Controls.Add(this.VQSortBy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.VQParam);
            this.Controls.Add(this.VQClear);
            this.Controls.Add(this.VQFilter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblViewQuote);
            this.Name = "ViewQuote";
            this.Text = "ViewQuote";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblViewQuote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox VQFilter;
        private System.Windows.Forms.Button VQClear;
        private System.Windows.Forms.TextBox VQParam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox VQSortBy;
        private System.Windows.Forms.Button VQGo;
        private System.Windows.Forms.ListBox VQlistQuote;
        private System.Windows.Forms.Button VQGenReport;
        private System.Windows.Forms.Button VQDisplayAll;
        private System.Windows.Forms.Button VQBack;
    }
}