using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PorterHireDesktopApp
{
    public partial class AddClient : Form
    {
        public AddClient()
        {
            InitializeComponent();

            label1.BackColor = System.Drawing.Color.Transparent;
            label2.BackColor = System.Drawing.Color.Transparent;
            label3.BackColor = System.Drawing.Color.Transparent;
            label4.BackColor = System.Drawing.Color.Transparent;
            label5.BackColor = System.Drawing.Color.Transparent;
            label6.BackColor = System.Drawing.Color.Transparent;
            label7.BackColor = System.Drawing.Color.Transparent;


            List<String> ClientsData = new List<String>();

            ClientsData.Add("Client1");
            ClientsData.Add("Client2");

            ClientCombo.DataSource = ClientsData;
        }

        private void ClientCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //do something
        }

        private void updateClientBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Client updated to database!");
            MessageBox.Show("Connection to database failed, Please try again!");
        }

        private void addClientBtn_Click(object sender, EventArgs e)
        {
            if (CFname.Text == String.Empty &&
                CLname.Text == String.Empty &&
                CEmail.Text == String.Empty &&
                CCompanyName.Text == String.Empty &&
                CPhone.Text == String.Empty)
            {
                MessageBox.Show("Please Fill All Required Fields!");
            }
            else
            {
                MessageBox.Show("Client added to database!");
                MessageBox.Show("Connection to database failed, Please try again!");
            }
        }

        private void deleteClientBtn_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you wish to delete this client?", "Warning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                MessageBox.Show("Client has been deleted from database!");
            }
            else if (dialogResult == DialogResult.No)
            {
                MessageBox.Show("No changes made");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new userMenu().Show();
            this.Hide();
        }
    }
}
