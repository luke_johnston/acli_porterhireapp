﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PorterHireDesktopApp
{
    public partial class ManageEquipment : Form
    {
        public ManageEquipment()
        {
            InitializeComponent();
            FuelTypePop();
            
        }

        void FuelTypePop()
        {
            
            StockFuelType.Items.Add("Petrol");
            StockFuelType.Items.Add("Diesel");
            StockFuelType.SelectedIndex = 0;
        }

        

        private void StockDescr_TextChanged(object sender, EventArgs e)
        {
            StockDescr.MaxLength = 50;
            string txtLength = StockDescr.Text.Length.ToString();
            txtStockMax.Text = "Maximum 50 Characters: (" + txtLength + "/50)";
        }

        private void StockBack_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }
    }
}
