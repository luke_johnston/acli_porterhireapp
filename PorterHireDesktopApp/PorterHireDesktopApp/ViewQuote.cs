﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PorterHireDesktopApp
{
    public partial class ViewQuote : Form
    {
        public ViewQuote()
        {
            InitializeComponent();
            FilterPop();
            SortByPop();
        }

        void FilterPop()
        {
            VQFilter.Items.Add("Model");
            VQFilter.Items.Add("Make");
            VQFilter.Items.Add("Client");
        }

        void SortByPop()
        {
            VQSortBy.Items.Add("Date");
            VQSortBy.Items.Add("ID");
            VQSortBy.Items.Add("Price");
        }

        private void VQClear_Click(object sender, EventArgs e)
        {
            VQParam.Text = "";
            VQFilter.Text = "";
        }

        private void VQBack_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }

        private void VQGo_Click(object sender, EventArgs e)
        {

        }
    }
}
