﻿namespace PorterHireDesktopApp
{
    partial class adminMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logoutBtn = new System.Windows.Forms.Button();
            this.armEquipmentBtn = new System.Windows.Forms.Button();
            this.mStaff = new System.Windows.Forms.Button();
            this.cQuoteBtn = new System.Windows.Forms.Button();
            this.vQuotesBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(812, 742);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(166, 59);
            this.logoutBtn.TabIndex = 7;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // armEquipmentBtn
            // 
            this.armEquipmentBtn.Location = new System.Drawing.Point(390, 385);
            this.armEquipmentBtn.Name = "armEquipmentBtn";
            this.armEquipmentBtn.Size = new System.Drawing.Size(204, 71);
            this.armEquipmentBtn.TabIndex = 6;
            this.armEquipmentBtn.Text = "Add, Remove, Modify Equipment";
            this.armEquipmentBtn.UseVisualStyleBackColor = true;
            this.armEquipmentBtn.Click += new System.EventHandler(this.armEquipmentBtn_Click);
            // 
            // mStaff
            // 
            this.mStaff.Location = new System.Drawing.Point(390, 243);
            this.mStaff.Name = "mStaff";
            this.mStaff.Size = new System.Drawing.Size(204, 71);
            this.mStaff.TabIndex = 5;
            this.mStaff.Text = "Manage Staff";
            this.mStaff.UseVisualStyleBackColor = true;
            this.mStaff.Click += new System.EventHandler(this.mStaff_Click);
            // 
            // cQuoteBtn
            // 
            this.cQuoteBtn.Location = new System.Drawing.Point(390, 109);
            this.cQuoteBtn.Name = "cQuoteBtn";
            this.cQuoteBtn.Size = new System.Drawing.Size(204, 71);
            this.cQuoteBtn.TabIndex = 4;
            this.cQuoteBtn.Text = "Create Quote";
            this.cQuoteBtn.UseVisualStyleBackColor = true;
            this.cQuoteBtn.Click += new System.EventHandler(this.cQuoteBtn_Click);
            // 
            // vQuotesBtn
            // 
            this.vQuotesBtn.Location = new System.Drawing.Point(390, 513);
            this.vQuotesBtn.Name = "vQuotesBtn";
            this.vQuotesBtn.Size = new System.Drawing.Size(204, 71);
            this.vQuotesBtn.TabIndex = 8;
            this.vQuotesBtn.Text = "View Quotes";
            this.vQuotesBtn.UseVisualStyleBackColor = true;
            this.vQuotesBtn.Click += new System.EventHandler(this.vQuotesBtn_Click);
            // 
            // adminMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.vQuotesBtn);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.armEquipmentBtn);
            this.Controls.Add(this.mStaff);
            this.Controls.Add(this.cQuoteBtn);
            this.Name = "adminMenu";
            this.Text = "adminMenu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button armEquipmentBtn;
        private System.Windows.Forms.Button mStaff;
        private System.Windows.Forms.Button cQuoteBtn;
        private System.Windows.Forms.Button vQuotesBtn;
    }
}