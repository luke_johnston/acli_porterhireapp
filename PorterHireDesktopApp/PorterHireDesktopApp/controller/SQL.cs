﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using PorterHireDesktopApp.setup;

namespace PorterHireDesktopApp.controller
{
    class SQL
    {
        public static SqlCommand cmd = new SqlCommand();
        public static SqlDataReader read;

        public static int executeQuery(string query)
        {
            //try catch to catch any unforseen errors gracefully
            try
            {
                connection.con.Close();
                cmd.Connection = connection.con;
                connection.con.Open();
                cmd.CommandText = query;
                var a = cmd.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine($"Success the result is: {a}");
                return a;
            }
            catch (Exception ex)
            {
                //put a message box in here if you are recieving errors and see if you can find out why?
                return 2;
            }
        }


        public static SqlDataReader selectQuery(string query)
        {
            try
            {
                connection.con.Close();
                cmd.Connection = connection.con;
                connection.con.Open();
                cmd.CommandText = query;
                read = cmd.ExecuteReader();

                return read;
            }
            catch (Exception ex)
            {
                ShowErrorMessage($"Something went wrong - {ex}");
                return null;
            }
        }
        public static string ShowErrorMessage(string message)
        {
            return message;
        }

        //public static void editComboBoxItems(ComboBox comboBox, string query)
        //{
        //    bool clear = true;

        //    //gets data from database
        //    SQL.selectQuery(query);
        //    //Check that there is something to write brah
        //    if (SQL.read.HasRows)
        //    {
        //        while (SQL.read.Read())
        //        {
        //            if (comboBox.Text == SQL.read[0].ToString())
        //            {
        //                clear = false;
        //            }
        //        }
        //    }

        //    //gets data from database
        //    SQL.selectQuery(query);
        //    //if nothing in the comboBox then we need to clear it
        //    if (clear)
        //    {
        //        comboBox.Text = "";
        //        comboBox.Items.Clear();

        //    }

        //    // this will print whatever is in the database to the combobox
        //    if (SQL.read.HasRows)
        //    {
        //        while (SQL.read.Read())
        //        {
        //            comboBox.Items.Add(SQL.read[0].ToString());
        //            System.Diagnostics.Debug.WriteLine(SQL.read[0].ToString());
        //        }
        //    }
        //}
    }
}
